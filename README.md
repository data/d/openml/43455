# OpenML dataset: Sloan-Digital-Sky-Survey-DR16

https://www.openml.org/d/43455

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Feedback: Mukharbek Organokov organokov.mgmail.com
Context
Sloan Digital Sky Survey current DR16 Server Data release with Galaxies, Stars and Quasars.
License:  Creative Commons Attribution license (CC-BY) More datailes here. Find more here.
Content
The table results from a query which joins two tables:   

"PhotoObj" which contains photometric data  
"SpecObj" which contains spectral data.

16 variables (double) and 1 additional variable (char) 'class'.
A class object can be predicted from the other 16 variables.
Variables description:  
objid = Object Identifier  
ra = J2000 Right Ascension (r-band)  
dec = J2000 Declination (r-band)  
u = better of deV/Exp magnitude fit (u-band)  
g = better of deV/Exp magnitude fit (g-band)  
r = better of deV/Exp magnitude fit (r-band)  
i = better of deV/Exp magnitude fit (i-band)  
z = better of deV/Exp magnitude fit (z-band)  
run = Run Number  
rerun = Rerun Number  
camcol = Camera column  
field = Field number  
specobjid = Object Identifier  
class = object class (galaxy, star or quasar object)  
redshift = Final Redshift  
plate = plate number  
mjd = MJD of observation  
fiberid = fiberID  
Comments

A four-color UVGR intermediate-band photometric system (Thuan-Gunn astronomic magnitude system) is discussed in [1]. The Sloan Digital Sky Survey (SDSS) photometric system, a new five-color (u g r i z) wide-band CCD system is described in [2]  
The variables 'run', 'rerun', 'camcol' and 'field' features which describe a field within an image taken by the SDSS. A field is basically a part of the entire image corresponding to 2048 by 1489 pixels. A field can be identified by: - run number, which identifies the specific scan, - the camera column, or "camcol," a number from 1 to 6, identifying the scanline within the run, and the field number. The field number typically starts at 11 (after an initial rampup time), and can be as large as 800 for particularly long runs. - An additional number, rerun, specifies how the image was processed.  
The variable 'class' identifies an object to be either a galaxy (GALAXY), star (STAR) or quasar (QSO).  

References:
[1] Thuan  Gunn (1976, PASP, 88,543)  
[2] Fukugita, M. et al, Astronomical J. v.111, p.1748  
Data server
Data can be obtained using SkyServer SQL Search with the command below:  
-- This query does a table JOIN between the imaging (PhotoObj) and spectra  
-- (SpecObj) tables and includes the necessary columns in the SELECT to upload  
-- the results to the SAS (Science Archive Server) for FITS file retrieval.  
SELECT TOP 100000  
   p.objid,p.ra,p.dec,p.u,p.g,p.r,p.i,p.z,  
   p.run, p.rerun, p.camcol, p.field,  
   s.specobjid, s.class, s.z as redshift,  
   s.plate, s.mjd, s.fiberid  
FROM PhotoObj AS p  
   JOIN SpecObj AS s ON s.bestobjid = p.objid  
WHERE     
   p.u BETWEEN 0 AND 19.6  
   AND g BETWEEN 0 AND 20  
Learn how to. Some examples. Full SQL Tutorial.  
Or perform a complicated, CPU-intensive query of SDSS catalog data using CasJobs, SQL-based interface to the CAS.    
Acknowledgements
SDSS collaboration.
Inspiration
The Sloan Digital Sky Survey has created the most detailed three-dimensional maps of the Universe ever made, with deep multi-color images of one-third of the sky, and spectra for more than three million astronomical objects. It allows to learn and explore all phases and surveys - past, present, and future - of the SDSS.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43455) of an [OpenML dataset](https://www.openml.org/d/43455). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43455/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43455/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43455/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

